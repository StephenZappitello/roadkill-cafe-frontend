import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../services/user/user';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-change-info',
  templateUrl: './change-info.component.html',
  styleUrls: ['./change-info.component.css']
})
export class ChangeInfoComponent implements OnInit {

  constructor(private router: Router, private userServ: UserService) { }


  user: User = new User();
  cfirstName = "";
  clastName = "";
  cemail = "";
  cpNum = "";
  cpassword = "";
  cpasswordcheck = "";
  status = "";
  currentPassword = "";

  get firstName(): string {
    return this.cfirstName;
  }

  set firstName(temp: string) {
    this.cfirstName = temp;
  }

  get lastName(): string {
    return this.clastName;
  }

  set lastName(temp: string) {
    this.clastName = temp;
  }

  get phoneNumber(): string {
    return this.cpNum;
  }

  set phoneNumber(temp: string) {
    this.cpNum = temp;
  }

  get email(): string {
    return this.cemail;
  }

  set email(temp: string) {
    this.cemail = temp;
  }

  get password(): string {
    return this.cpassword;
  }

  set password(temp: string) {
    this.cpassword = temp;
  }

  get passwordcheck(): string {
    return this.cpasswordcheck;
  }

  set passwordcheck(temp: string) {
    this.cpasswordcheck = temp;
  }





  postChangeInfo() {


    this.user.email = this.email;
    this.user.firstName = this.firstName;
    this.user.lastName = this.lastName;
    this.user.phoneNumber = this.phoneNumber;
    if (this.password !== this.passwordcheck) {
      alert('Passwords do not match');
      return;
    } else if (this.password.length < 8 && this.password.length > 0) {
      alert("Password must be at least 8 characters");
      return;
    } else if (this.password.length === 0) {
      this.user.password = this.currentPassword;
    }
    else {
      this.user.password = this.password;
    }

    this.userServ.updateUser(this.user).subscribe(
      data => {
        alert("Information Successfully Changed!");
        this.router.navigate(['/welcome']);
      },
      error => {
        alert(error.error);
      }
    );
  }

  
  ngOnInit(): void {
    this.userServ.getCurrentUser(localStorage.getItem('currentUser')).subscribe(
      response => {
        console.log(response);
        this.user = response;
        this.cfirstName = response.firstName;
        this.clastName = response.lastName;
        this.cemail = response.email;
        this.cpNum = response.phoneNumber;
        this.currentPassword = response.password;
      }
    );
  }

}
