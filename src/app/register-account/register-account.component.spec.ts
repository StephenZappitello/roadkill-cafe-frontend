import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginComponent } from '../login/login.component';
import { UserService } from '../services/user/user.service';
import { Location } from '@angular/common';

import { RegisterAccountComponent } from './register-account.component';
import { By } from '@angular/platform-browser';
import { Observable } from 'rxjs';

describe('RegisterAccountComponent', () => {

  class MockService {
    createUser(){}
  }

  let dummyUserData = {
    customerId: 1,
    username: 'testuser',
    password: 'testpass',
    email: 'test@test.com',
    firstName: 'testfname', 
    lastName: 'testlastname',
    phoneNumber: '0001110000'
  }

  let component: RegisterAccountComponent;
  let fixture: ComponentFixture<RegisterAccountComponent>;
  let router: Router;
  let location: Location;
  let userService: UserService;
  let mockClient: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy};


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: '', component: RegisterAccountComponent},
          {path: 'login', component: LoginComponent}
        ]),
        FormsModule,
      ],
      declarations: [ RegisterAccountComponent ],
      providers: [
        {provide: UserService, useClass: MockService},
        {provide: HttpClient, useValue: mockClient},
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegisterAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.username = dummyUserData.username;
    component.password = dummyUserData.password;
    component.confPassword = dummyUserData.password;
    component.email = dummyUserData.email;
    component.firstName = dummyUserData.firstName;
    component.lastName = dummyUserData.lastName;
    component.phoneNumber = dummyUserData.phoneNumber;

    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    userService = TestBed.inject(UserService);
    mockClient = TestBed.get(HttpClient);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have h3 say Register New Account', ()=>{
    fixture.detectChanges();
    let head = fixture.debugElement.query(By.css('h3')).nativeElement;
    expect(head.innerHTML).toBe('Register New Account');
  });

  it('should call the postCreateAccount() method', waitForAsync(()=>{
    fixture.detectChanges();
    let createAccountButton = fixture.debugElement.query(By.css('#submitbutton')).nativeElement;
    spyOn(component, 'postCreateAccount');
    createAccountButton.click();

    fixture.whenStable().then(()=>{
      expect(component.postCreateAccount).toHaveBeenCalled();
    });
  }));

  it('should redirect to login component after calling the createUser() method async', waitForAsync(()=>{
    fixture.detectChanges();
    let createAccountButton = fixture.debugElement.query(By.css('#submitbutton')).nativeElement;

    let userServiceSpy = spyOn(userService, 'createUser').and.returnValue(Observable.create(observer=>{
      observer.next("asd");
    }));

    createAccountButton.click();

    fixture.detectChanges();

    fixture.whenStable().then(()=>{
      expect(location.path()).toEqual('/login');
    });
  }));


});
