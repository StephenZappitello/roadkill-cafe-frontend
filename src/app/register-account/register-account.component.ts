import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../services/user/user';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-register-account',
  templateUrl: './register-account.component.html',
  styleUrls: ['./register-account.component.css']
})
export class RegisterAccountComponent implements OnInit {

  constructor(private router: Router, private userServ: UserService) { }

  loginName = "";
  loginPassword = "";
  loginPasswordCheck = "";
  loginEmail = "";
  fName = "";
  lName = "";
  pNum = "";
  status = "";

  get username():string{
    return this.loginName;
  }

  set username(temp:string){
    this.loginName = temp;
  }

  get password():string{
    return this.loginPassword;
  }

  set password(temp:string){
    this.loginPassword = temp;
  }

  get confPassword():string{
    return this.loginPasswordCheck;
  }

  set confPassword(temp:string){
    this.loginPasswordCheck = temp;
  }

  get email():string{
    return this.loginEmail;
  }

  set email(temp:string){
    this.loginEmail = temp;
  }

  get firstName():string {
    return this.fName;
  }

  set firstName(temp:string) {
    this.fName = temp;
  }

  get lastName():string {
    return this.lName;
  }

  set lastName(temp:string) {
    this.lName = temp;
  }

  get phoneNumber():string {
    return this.pNum;
  }
 
  set phoneNumber(temp:string) {
    this.pNum = temp;
  }

  postCreateAccount(){
    var user = new User();
    user.username = this.username;
    user.email = this.email;
    user.firstName = this.firstName;
    user.lastName = this.lastName;
    user.phoneNumber = this.phoneNumber;
    if (this.password.length<8){
      alert("Password must be at least 8 characters");
      return;
    }
    if(this.password !== this.confPassword){
      alert("Passwords do not match");
      return;
    }
    user.password = this.password;
    this.userServ.createUser(user).subscribe(
      data => {
        alert("Account Successfully Created");
        this.router.navigate(['/login']);
      },
      error => {
        alert(error.error);
      }
    );
  }

  ngOnInit(): void {
  }

}
