import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../services/user/user';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private router: Router, private userServ: UserService) { }

  loginEmail = ""; 

  get email():string{ 
    return this.loginEmail;
  }

  set email(temp:string){
    this.loginEmail = temp;
  }

  postRecoverPassword() {
    var user = new User();
    user.email = this.email;
    console.log(user);

    this.userServ.recoverPassword(user).subscribe(
      response => {
        alert("Recovery email with your new password sent!");
        console.log(response);
      },
      error => {
        alert("No user with that email exists.");
      }
    );

  }

  ngOnInit(): void {
  }

}
