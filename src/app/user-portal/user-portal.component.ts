import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../services/user/user';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-user-portal',
  templateUrl: './user-portal.component.html',
  styleUrls: ['./user-portal.component.css']
})
export class UserPortalComponent implements OnInit {

  constructor(private router: Router, private userServ: UserService) { }

  user: User = new User();
  cfirstName = "";

  get firstName(): string {
    return `Welcome back, ${this.cfirstName}.`;
  }
 
  set firstName(temp: string) {
    this.cfirstName = temp;
  }

  ngOnInit(): void {
    this.userServ.getCurrentUser(this.getCurrentUsername()).subscribe(
      response => {
        console.log(response);
        this.user = response;
        this.cfirstName = response.firstName;
      }
    );
  }

  getCurrentUsername(): string {
    return localStorage.getItem('currentUser');
  }

}
