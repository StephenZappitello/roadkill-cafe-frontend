import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../services/user/user';
import { Router } from '@angular/router';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html', 
  styleUrls: ['./login.component.css'] 
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private userServ: UserService) { }

  loginName = "";
  loginPassword = "";
  status="";

  get username():string{
    return this.loginName;
  }

  set username(temp:string) {
    this.loginName = temp;
  }

  get password():string{
    return this.loginPassword;
  }

  set password(temp:string) {
    this.loginPassword = temp;
  }

  postLogin() {
    var user = new User();
    user.username = this.username;
    user.password = this.password;
    console.log('in login');

    this.userServ.login(user).subscribe(
      data => {
        localStorage.setItem('currentUser', this.username);
        console.log('trying to route');
        this.router.navigate(['/welcome'])
          .then(()=>{
            this.windowReload();
          });
      },
      error => {
        this.status = error.error;
      }
    );
  }

  windowReload() {
    window.location.reload();
  }

  ngOnInit(): void {
  }

}
