import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { faShoppingCart, faUser } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  username = localStorage.getItem("currentUser");

  //loggedin = false;

  constructor(private router: Router) {}
  
  title = 'roadkill-cafe-frontend';
  faShoppingCart = faShoppingCart;
  faUser = faUser;
  
  
  logout(){
    localStorage.removeItem("currentUser");
    location.reload();

    this.router.navigate(['/welcome'])
          .then(()=>{
            window.location.reload();
          });
  }
}

