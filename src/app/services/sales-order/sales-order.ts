import { IInvoiceItemComposite } from "../invoice-item-composite/invoice-item-composite";

export interface ISalesOrder{
    orderDate:string;
    orderId:number;
    invoiceItems:IInvoiceItemComposite[];
    totalPrice:number;
}