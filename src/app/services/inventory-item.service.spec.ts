import { TestBed } from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing'
import { InventoryItemService } from './inventory-item.service';

const dummyInventoryItemData = {
  itemId: 1,
  productName: 'test',
  salesPrice: 100,
  description: 'test',
  menuImg: 'test',
  quantity: 0
};

const dummyInventoryItem = {
  itemId: 1,
  productName: 'item',
  salesPrice: 1,
  description: 'description',
  menuImg: 'image',
  quantity: 1
};

describe('InventoryItemService', () => {
  let service: InventoryItemService;
  let httpMock: HttpTestingController;
 
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      providers: [InventoryItemService],
    });
    service = TestBed.inject(InventoryItemService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(()=> {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have getAllItems() return data', ()=>{
    service.getAllItems().subscribe(
      response => {
        expect(response.toString()).toEqual(dummyInventoryItemData.toString());
      });

      const req = httpMock.expectOne("http://localhost:9025/inventory/all");
      expect(req.request.method).toBe('GET');
      req.flush(dummyInventoryItemData);
  });
});
