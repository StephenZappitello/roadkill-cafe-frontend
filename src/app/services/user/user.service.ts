import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpCli: HttpClient) { }

  baseURL: string = "http://ec2-3-23-20-124.us-east-2.compute.amazonaws.com:9025/";

  

  createUser(user: User): Observable<any> {
    var headers = { 'content-type': 'application/json'};
    var body = JSON.stringify(user);
  
    return this.httpCli.post(this.baseURL + 'users/create', body, {'headers':headers, responseType: 'text'});
  }

  updateUser(user:User): Observable<any>{
    var headers = { 'content-type': 'application/json'};
    var body = JSON.stringify(user);
  
    return this.httpCli.post(this.baseURL + 'users/update', body, {'headers':headers, responseType: 'text'});

  }

  login(user: User): Observable<any> {
    var headers = { 'content-type': 'application/json'};
    var body = JSON.stringify(user);
    console.log(body);
    
    return this.httpCli.post(this.baseURL + 'users/login', body, {'headers':headers, responseType: 'text'});
  }

  recoverPassword(user: User): Observable<any> {
    var headers = { 'content-type': 'application/json'};
    var body = JSON.stringify(user);
    console.log(body);
    
    return this.httpCli.post(this.baseURL + 'users/recover', body, {'headers':headers, responseType: 'text'});
  }

  getCurrentUser(username:string): Observable<User>{
    return this.httpCli.get<User>(this.baseURL + 'users/' + username);
  }




}
