import { TestBed } from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing'
import { UserService } from './user.service';

const dummyUserData = {
  customerId: 1,
  username: 'testuser',
  password: 'testpass',
  email: 'test@test.com',
  firstName: 'testfname', 
  lastName: 'testlastname',
  phoneNumber: '0001110000'
}

const dummyLoginResponse = "Login successful";
const dummyRecoverPasswordResponse = "Recovery email sent";
const dummyUpdateUserResponse = "Successfully updated";
const dummyCreateUserResponse = "Resource created";

describe('UserService', () => {
  let service: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      providers: [UserService],
    });
    service = TestBed.inject(UserService);
    httpMock = TestBed.inject(HttpTestingController);
  }); 

  afterEach(()=>{
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have getCurrentUser(username: string) return data', ()=>{
    service.getCurrentUser('testuser').subscribe(
      response => {
        console.log(response);
        expect(response.toString()).toEqual(dummyUserData.toString());
      });

      const req = httpMock.expectOne("http://localhost:9025/users/testuser");
      expect(req.request.method).toBe('GET');
      req.flush(dummyUserData);
    
  });

  it('should have login(user: User) return a status string', ()=>{
    service.login(dummyUserData).subscribe(
      response => {
        console.log(response);
        expect(response.toString()).toEqual(dummyLoginResponse);
      });

      const req = httpMock.expectOne("http://localhost:9025/users/login");
      expect(req.request.method).toBe('POST');
      req.flush(dummyLoginResponse);
  });

  it('should have recoverPassword(user: User) return a status string', ()=>{
    service.recoverPassword(dummyUserData).subscribe(
      response => {
        console.log(response);
        expect(response.toString()).toEqual(dummyRecoverPasswordResponse);
      });

      const req = httpMock.expectOne("http://localhost:9025/users/recover");
      expect(req.request.method).toBe('POST');
      req.flush(dummyRecoverPasswordResponse);
  });

  it('should have updateUser(user: User) return a status string', ()=>{
    service.updateUser(dummyUserData).subscribe(
      response => {
        console.log(response);
        expect(response.toString()).toEqual(dummyUpdateUserResponse);
      });

      const req = httpMock.expectOne("http://localhost:9025/users/update");
      expect(req.request.method).toBe('POST');
      req.flush(dummyUpdateUserResponse);
  });

  it('should have createUser(user: User) return a status string', ()=>{
    service.createUser(dummyUserData).subscribe(
      response => {
        console.log(response);
        expect(response.toString()).toEqual(dummyCreateUserResponse);
      });

      const req = httpMock.expectOne("http://localhost:9025/users/create");
      expect(req.request.method).toBe('POST');
      req.flush(dummyCreateUserResponse);
  });

});
