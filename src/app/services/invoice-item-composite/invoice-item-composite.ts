export interface IInvoiceItemComposite{
    inventoryProductName:string;
    inventoryImage:string;
    invoiceQuantity:number;
}